#!/bin/bash

function stopDocker() {
        echo "Shutting down docker."
        sudo docker-compose down
        echo "Stopped bash process."
        exit 2
}

trap "stopDocker" 2

mvn clean install && docker-compose build && docker-compose up
