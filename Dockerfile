FROM maven:3.5-jdk-8
FROM java:8
WORKDIR /.
COPY target/school-registration-sys*.jar /app/school-registration-sys.jar
ENTRYPOINT ["java","-jar","-Dspring.profiles.active=default,prod","/app/school-registration-sys.jar"]
