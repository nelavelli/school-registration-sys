package com.mdi.srs.business.service.impl;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.fail;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.anyList;
import static org.mockito.Mockito.anySet;
import static org.mockito.Mockito.any;
import org.mockito.MockitoAnnotations;
import org.springframework.data.jpa.domain.Specification;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.mdi.srs.entity.CourseEntity;
import com.mdi.srs.entity.StudentEntity;
import com.mdi.srs.enums.ErrorCode;
import com.mdi.srs.exception.SRSProcessingException;
import com.mdi.srs.model.Course;
import com.mdi.srs.model.SearchFilter;
import com.mdi.srs.model.Student;
import com.mdi.srs.repository.SrsBaseRepository;

public class SrsServiceImplTest {

	private Long sId = 1l;

	private Long cId = 1l;

	private long[] studentIDs = { 1, 2 };

	private long[] courseIds = { 1, 2 };

	@Mock
	private SrsBaseRepository<StudentEntity, Long> studentRepository;

	@Mock
	private SrsBaseRepository<CourseEntity, Long> courseRepository;

	@InjectMocks
	private SrsServiceImpl<Student, StudentEntity, CourseEntity> srsStudentService;

	@InjectMocks
	private SrsServiceImpl<Course, CourseEntity, StudentEntity> srsCourseService;

	@BeforeMethod
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	private SrsServiceImpl<Student, StudentEntity, CourseEntity> initiateStudentServiceMock() {
		return new SrsServiceImpl<Student, StudentEntity, CourseEntity>(studentRepository, courseRepository,
				Student.class, StudentEntity.class);
	}

	private SrsServiceImpl<Course, CourseEntity, StudentEntity> initiateCourseServiceMock() {
		return new SrsServiceImpl<Course, CourseEntity, StudentEntity>(courseRepository, studentRepository,
				Course.class, CourseEntity.class);
	}

	@Test
	public void deregisterStudentTest() {
		srsStudentService = this.initiateStudentServiceMock();
		when(studentRepository.findById(anyLong())).thenReturn(Optional.of(getStudentEntity()));
		doNothing().when(studentRepository).deleteById(anyLong());
		srsStudentService.deregister(sId);
		verify(studentRepository, times(1)).deleteById(anyLong());
		verify(studentRepository, times(1)).findById(anyLong());
	}

	@Test
	public void deregisterStudentDataNotFoundTest() {
		try {
			srsStudentService = this.initiateStudentServiceMock();
			when(studentRepository.findById(anyLong())).thenThrow(new SRSProcessingException(ErrorCode.DATA_NOT_FOUND));
			doNothing().when(studentRepository).deleteById(anyLong());
			srsStudentService.deregister(sId);
		} catch (SRSProcessingException srspEx) {
			assertEquals(srspEx.getMessage(), ErrorCode.DATA_NOT_FOUND.getMessage());
		}
		verify(studentRepository, times(1)).findById(anyLong());
	}

	@Test
	public void deregisterCourseTest() {
		srsCourseService = this.initiateCourseServiceMock();
		when(courseRepository.findById(anyLong())).thenReturn(Optional.of(getCourseEntity()));
		doNothing().when(courseRepository).deleteById(anyLong());
		srsCourseService.deregister(cId);
		verify(courseRepository, times(1)).deleteById(anyLong());
		verify(courseRepository, times(1)).findById(anyLong());
	}

	@Test
	public void deregisterCourseDataNotFoundTest() {
		try {
			srsCourseService = this.initiateCourseServiceMock();
			when(courseRepository.findById(anyLong())).thenThrow(new SRSProcessingException(ErrorCode.DATA_NOT_FOUND));
			doNothing().when(courseRepository).deleteById(anyLong());
			srsCourseService.deregister(cId);
		} catch (SRSProcessingException srspEx) {
			assertEquals(srspEx.getMessage(), ErrorCode.DATA_NOT_FOUND.getMessage());
		}
		verify(courseRepository, times(1)).findById(anyLong());
	}

	@Test
	public void enrollCourseForStudentsTest() {

		srsCourseService = this.initiateCourseServiceMock();
		when(courseRepository.findById(anyLong())).thenReturn(Optional.of(getCourseEntity()));
		when(studentRepository.findAllById(LongStream.of(studentIDs).boxed().collect(Collectors.toList())))
				.thenReturn(getStudentEntities());

		srsCourseService.enrollCourseForStudents(anyLong(),
				LongStream.of(studentIDs).boxed().collect(Collectors.toList()));
		verify(courseRepository, times(1)).findById(anyLong());
		verify(studentRepository, times(1)).findAllById(anyList());
	}

	@Test
	public void enrollCourseForStudentsExceedTest() {
		try {
			srsCourseService = this.initiateCourseServiceMock();
			when(courseRepository.findById(anyLong())).thenReturn(Optional.of(getCourseEntity()));
			when(studentRepository.findAllById(anyList()))
					.thenThrow(new SRSProcessingException(ErrorCode.STUDENT_LIMIT_EXCEEDS));
			List<Long> studentIds = IntStream.range(1, 52).mapToLong(x -> x).collect(ArrayList<Long>::new,
					ArrayList::add, ArrayList::addAll);
			srsCourseService.enrollCourseForStudents(cId, studentIds);
			fail("control should not reach to this line.");
		} catch (SRSProcessingException srspEx) {
			assertEquals(srspEx.getMessage(), ErrorCode.STUDENT_LIMIT_EXCEEDS.getMessage());
		}
		verify(courseRepository, times(1)).findById(anyLong());
		verify(studentRepository, times(0)).findAllById(anyList());
	}

	@Test
	public void enrollCourseForStudentsNotFoundTest() {
		try {
			srsCourseService = this.initiateCourseServiceMock();
			when(courseRepository.findById(anyLong())).thenReturn(Optional.of(getCourseEntity()));
			when(studentRepository.findAllById(anyList())).thenThrow(new SRSProcessingException(
					ErrorCode.DATA_NOT_FOUND, "Student Id(s) 3, 4 Not found in the System."));
			List<Long> studentIds = IntStream.range(1, 5).mapToLong(x -> x).collect(ArrayList<Long>::new,
					ArrayList::add, ArrayList::addAll);
			srsCourseService.enrollCourseForStudents(cId, studentIds);
			fail("control should not reach to this line.");
		} catch (SRSProcessingException srspEx) {
			assertEquals(srspEx.getMessage(), "Student Id(s) 3, 4 Not found in the System.");
		}
		verify(courseRepository, times(1)).findById(anyLong());
		verify(studentRepository, times(1)).findAllById(anyList());
	}

	@Test
	public void enrollSutdentForCoursesTest() {

		srsStudentService = this.initiateStudentServiceMock();
		when(studentRepository.findById(anyLong())).thenReturn(Optional.of(getStudentEntity()));
		when(courseRepository.findAllById(LongStream.of(courseIds).boxed().collect(Collectors.toList())))
				.thenReturn(getCourseEntities());

		srsStudentService.enrollSutdentForCourses(sId, LongStream.of(courseIds).boxed().collect(Collectors.toList()));
		verify(studentRepository, times(1)).findById(anyLong());
		verify(courseRepository, times(1)).findAllById(anyList());
	}

	@Test
	public void enrollSutdentForCoursesExceedTest() {
		try {
			srsStudentService = this.initiateStudentServiceMock();
			when(studentRepository.findById(anyLong())).thenReturn(Optional.of(getStudentEntity()));
			when(courseRepository.findAllById(anyList()))
					.thenThrow(new SRSProcessingException(ErrorCode.COURSE_LIMIT_EXCEEDS));
			List<Long> courseIds = IntStream.range(1, 7).mapToLong(x -> x).collect(ArrayList<Long>::new, ArrayList::add,
					ArrayList::addAll);
			srsStudentService.enrollSutdentForCourses(sId, courseIds);
			fail("control should not reach to this line.");
		} catch (SRSProcessingException srspEx) {
			assertEquals(srspEx.getMessage(), ErrorCode.COURSE_LIMIT_EXCEEDS.getMessage());
		}
		verify(studentRepository, times(1)).findById(anyLong());
		verify(courseRepository, times(0)).findAllById(anyList());
	}

	@Test
	public void enrollSutdentForCoursesNotFoundTest() {
		try {
			srsStudentService = this.initiateStudentServiceMock();
			when(studentRepository.findById(anyLong())).thenReturn(Optional.of(getStudentEntity()));
			when(courseRepository.findAllById(anyList())).thenThrow(
					new SRSProcessingException(ErrorCode.DATA_NOT_FOUND, "Course Id(s) 3, 4 Not found in the System"));
			List<Long> courseIds = IntStream.range(1, 5).mapToLong(x -> x).collect(ArrayList<Long>::new, ArrayList::add,
					ArrayList::addAll);
			srsStudentService.enrollSutdentForCourses(sId, courseIds);
			fail("control should not reach to this line.");
		} catch (SRSProcessingException srspEx) {
			assertEquals(srspEx.getMessage(), "Course Id(s) 3, 4 Not found in the System");
		}
		verify(studentRepository, times(1)).findById(anyLong());
		verify(courseRepository, times(1)).findAllById(anyList());
	}

	@Test
	public void studentDataNotFoundTest() {
		try {
			srsStudentService = this.initiateStudentServiceMock();
			when(studentRepository.findById(anyLong())).thenThrow(new SRSProcessingException(ErrorCode.DATA_NOT_FOUND));
			srsStudentService.find(sId);
			fail("control should not reach to this line.");
		} catch (SRSProcessingException srspEx) {
			assertEquals(srspEx.getMessage(), ErrorCode.DATA_NOT_FOUND.getMessage());
		}
		verify(studentRepository, times(1)).findById(anyLong());
	}

	@Test
	public void courseDataNotFoundTest() {
		try {
			srsCourseService = this.initiateCourseServiceMock();
			when(courseRepository.findById(anyLong())).thenThrow(new SRSProcessingException(ErrorCode.DATA_NOT_FOUND));
			srsStudentService.find(cId);
			fail("control should not reach to this line.");
		} catch (SRSProcessingException srspEx) {
			assertEquals(srspEx.getMessage(), ErrorCode.DATA_NOT_FOUND.getMessage());
		}
	}

	@Test
	public void findCourseTest() {
		srsCourseService = this.initiateCourseServiceMock();
		when(courseRepository.findById(anyLong())).thenReturn(Optional.of(getCourseEntity()));
		Course course = srsCourseService.find(cId);
		assertEquals(course, getCourse());
		verify(courseRepository, times(1)).findById(anyLong());
	}

	@Test
	public void findStudentTest() {
		srsStudentService = this.initiateStudentServiceMock();
		when(studentRepository.findById(anyLong())).thenReturn(Optional.of(getStudentEntity()));
		Student student = srsStudentService.find(sId);
		assertEquals(student, getStudent());
		verify(studentRepository, times(1)).findById(anyLong());
	}

	@Test
	public void findAllCourseTest() {
		srsCourseService = this.initiateCourseServiceMock();
		when(courseRepository.findAll()).thenReturn(getCourseEntities());
		List<Course> courses = srsCourseService.findAll();
		assertEquals(courses, getCourses());
		verify(courseRepository, times(1)).findAll();
	}

	@Test
	public void findAllStudentTest() {
		srsStudentService = this.initiateStudentServiceMock();
		when(studentRepository.findAll()).thenReturn(getStudentEntities());
		List<Student> students = srsStudentService.findAll();
		assertEquals(students, getStudents());
		verify(studentRepository, times(1)).findAll();
	}

	@Test
	public void registerCourseTest() {
		srsCourseService = this.initiateCourseServiceMock();
		CourseEntity expectedCourse = new CourseEntity();
		expectedCourse.setCourseName("Java and J2ee");
		expectedCourse.setId(cId);
		when(courseRepository.save(any(CourseEntity.class))).thenReturn(expectedCourse);

		Course course = new Course();
		course.setCourseName("Java and J2ee");

		Course output = srsCourseService.register(course);
		assertEquals(output, getCourse());
		verify(courseRepository, times(1)).save(any(CourseEntity.class));
	}

	@Test
	public void registerStudentTest() {
		srsStudentService = this.initiateStudentServiceMock();
		when(studentRepository.save(any(StudentEntity.class))).thenReturn(getStudentEntity());

		Student student = new Student();
		student.setFirstName("Jon");
		student.setLastName("Snow");

		Student output = srsStudentService.register(student);
		assertEquals(output, getStudent());
		verify(studentRepository, times(1)).save(any(StudentEntity.class));
	}

	@Test
	public void registerAllCoursesTest() {
		srsCourseService = this.initiateCourseServiceMock();
		when(courseRepository.saveAll(anySet())).thenReturn(getCourseEntities());

		List<Course> courses = srsCourseService.registerAll(getRegCourses());
		assertEquals(courses, getCourses());
		verify(courseRepository, times(1)).saveAll(anySet());
	}

	@Test
	public void registerAllStudentsTest() {
		srsStudentService = this.initiateStudentServiceMock();
		when(studentRepository.saveAll(anySet())).thenReturn(getStudentEntities());

		List<Student> students = srsStudentService.registerAll(getRegStudents());
		assertEquals(students, getStudents());
		verify(studentRepository, times(1)).saveAll(anySet());
	}

	@Test
	public void searchSrsCourseSystemTest() {

		srsStudentService = this.initiateStudentServiceMock();
		SearchFilter searchFilter = new SearchFilter();
		searchFilter.setCourseName("Java and J2ee");
		searchFilter.setCourseWithoutStudent(true);
		when(courseRepository.findAll(any(Specification.class))).thenReturn(getCourseEntities());

		List<?> output = srsStudentService.searchSrsSystem(searchFilter);
		assertEquals(output, getCourses());
		verify(courseRepository, times(1)).findAll(any(Specification.class));
	}

	@Test
	public void searchSrsStudentSystemTest() {
		srsStudentService = this.initiateStudentServiceMock();

		SearchFilter searchFilter = new SearchFilter();
		searchFilter.setFirstName("Jon");
		searchFilter.setLastName("Snow");
		when(studentRepository.findAll(any(Specification.class))).thenReturn(getStudentEntities());

		List<?> output = srsStudentService.searchSrsSystem(searchFilter);
		List<Student> students = getStudents().stream().map(student -> {
			student.setCourses(new HashSet<Course>());
			return student;
		}).collect(Collectors.toList());
		assertEquals(output, students);

		verify(studentRepository, times(1)).findAll(any(Specification.class));
	}

	@Test
	public void searchSrsSystemisCourseWithoutStudentTest() {
		srsStudentService = this.initiateStudentServiceMock();
		SearchFilter searchFilter = new SearchFilter();
		searchFilter.setCourseWithoutStudent(false);
		when(studentRepository.findAll(any(Specification.class))).thenReturn(getStudentEntities());
		List<?> output = srsStudentService.searchSrsSystem(searchFilter);
		verify(studentRepository, times(1)).findAll(any(Specification.class));
	}

	@Test
	public void updateCourseTest() {
		srsCourseService = this.initiateCourseServiceMock();
		CourseEntity expectedCourse = new CourseEntity();
		expectedCourse.setCourseName("Java and J2ee-updated");
		expectedCourse.setId(cId);
		when(courseRepository.findById(anyLong())).thenReturn(Optional.of(expectedCourse));
		when(courseRepository.save(any(CourseEntity.class))).thenReturn(expectedCourse);

		Course course = new Course();
		course.setCourseName("Java and J2ee-updated");

		Course output = srsCourseService.update(anyLong(), course);
		course.setId(cId);
		assertEquals(output, course);
		verify(courseRepository, times(1)).save(any(CourseEntity.class));
		verify(courseRepository, times(1)).findById(anyLong());
	}

	@Test
	public void updateCourseDataNotFoundTest() {
		try {
			srsCourseService = this.initiateCourseServiceMock();
			when(courseRepository.findById(anyLong())).thenThrow(new SRSProcessingException(ErrorCode.DATA_NOT_FOUND));
			Course course = new Course();
			course.setCourseName("Java and J2ee-updated");
			srsCourseService.update(cId, course);
		} catch (SRSProcessingException srspEx) {
			assertEquals(srspEx.getMessage(), ErrorCode.DATA_NOT_FOUND.getMessage());
		}
		verify(courseRepository, times(1)).findById(anyLong());
		verify(courseRepository, times(0)).save(any(CourseEntity.class));
	}

	@Test
	public void updateStudentTest() {
		srsStudentService = this.initiateStudentServiceMock();
		StudentEntity expectedStudent = new StudentEntity();
		expectedStudent.setId(sId);
		expectedStudent.setFirstName("Jon-updated");
		expectedStudent.setLastName("Snow");
		when(studentRepository.findById(anyLong())).thenReturn(Optional.of(expectedStudent));
		when(studentRepository.save(any(StudentEntity.class))).thenReturn(expectedStudent);

		Student student = new Student();
		student.setFirstName("Jon-updated");
		student.setLastName("Snow");

		Student output = srsStudentService.update(sId, student);
		student.setId(sId);
		assertEquals(output, student);
		verify(studentRepository, times(1)).save(any(StudentEntity.class));
		verify(studentRepository, times(1)).findById(anyLong());
	}

	@Test
	public void updateStudentDataNotFoundTest() {
		try {
			srsStudentService = this.initiateStudentServiceMock();
			when(studentRepository.findById(anyLong())).thenThrow(new SRSProcessingException(ErrorCode.DATA_NOT_FOUND));
			Student student = new Student();
			student.setFirstName("Jon-updated");
			student.setLastName("Snow");
			srsStudentService.update(sId, student);
		} catch (SRSProcessingException srspEx) {
			assertEquals(srspEx.getMessage(), ErrorCode.DATA_NOT_FOUND.getMessage());
		}
		verify(studentRepository, times(1)).findById(anyLong());
		verify(studentRepository, times(0)).save(any(StudentEntity.class));
	}

	private StudentEntity getStudentEntity() {
		StudentEntity se1 = new StudentEntity();
		se1.setId(sId);
		se1.setFirstName("Jon");
		se1.setLastName("Snow");
		return se1;
	}

	private List<StudentEntity> getStudentEntities() {
		List<StudentEntity> studentList = new ArrayList<>();
		StudentEntity se1 = new StudentEntity();
		se1.setId(1l);
		se1.setFirstName("Jon");
		se1.setLastName("Snow");

		StudentEntity se2 = new StudentEntity();
		se2.setId(2l);
		se2.setFirstName("Harvey");
		se2.setLastName("Specter");
		studentList.add(se1);
		studentList.add(se2);
		return studentList;
	}

	private Student getStudent() {
		Student s = new Student();
		s.setId(1l);
		s.setFirstName("Jon");
		s.setLastName("Snow");
		return s;
	}

	private Course getCourse() {
		Course c = new Course();
		c.setId(1l);
		c.setCourseName("Java and J2ee");
		return c;
	}

	private CourseEntity getCourseEntity() {
		CourseEntity c = new CourseEntity();
		c.setId(1l);
		c.setCourseName("Java and J2ee");
		return c;
	}

	private List<CourseEntity> getCourseEntities() {
		List<CourseEntity> courses = new ArrayList<>();

		CourseEntity course1 = new CourseEntity();
		course1.setId(1l);
		course1.setCourseName("Java and J2ee");

		CourseEntity course2 = new CourseEntity();
		course2.setId(2l);
		course2.setCourseName("AWS Developer");
		courses.add(course1);
		courses.add(course2);
		return courses;
	}

	private List<Student> getStudents() {
		List<Student> studentList = new ArrayList<>();
		Student student1 = new Student();
		student1.setId(1l);
		student1.setFirstName("Jon");
		student1.setLastName("Snow");

		Student student2 = new Student();
		student2.setId(2l);
		student2.setFirstName("Harvey");
		student2.setLastName("Specter");
		studentList.add(student1);
		studentList.add(student2);
		return studentList;
	}

	private List<Course> getCourses() {
		List<Course> courses = new ArrayList<>();
		Course course1 = new Course();
		course1.setId(1l);
		course1.setCourseName("Java and J2ee");

		Course course2 = new Course();
		course2.setId(2l);
		course2.setCourseName("AWS Developer");
		courses.add(course1);
		courses.add(course2);
		return courses;
	}

	private Set<Course> getRegCourses() {
		return new HashSet<Course>(getCourses());
	}

	private Set<Student> getRegStudents() {
		return new HashSet<>(getStudents());
	}

}
