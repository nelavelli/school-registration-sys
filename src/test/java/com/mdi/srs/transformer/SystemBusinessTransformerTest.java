package com.mdi.srs.transformer;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.testng.annotations.Test;

import com.mdi.srs.entity.CourseEntity;
import com.mdi.srs.entity.StudentEntity;
import com.mdi.srs.enums.ErrorCode;
import com.mdi.srs.exception.SRSProcessingException;
import com.mdi.srs.model.Course;
import com.mdi.srs.model.Student;

public class SystemBusinessTransformerTest {

	@Test
	public void tansformToSystemObjectPojoTest() {
		Course course = SystemBusinessTransformer.tansformToSystemObject(getCourseEntity(), Course.class);
		assertEquals(course, getCourse());
	}

	@Test
	public void tansformToSystemObjectTest() {
		CourseEntity ce = SystemBusinessTransformer.tansformToSystemObject(getCourse(), CourseEntity.class);
		assertNotNull(ce);
	}

	@Test
	public void tansformToSystemObjectExceptionTest() {
		try {
			SystemBusinessTransformer.tansformToSystemObject(null, CourseEntity.class);
		} catch (SRSProcessingException spe) {
			assertEquals(spe.getMessage(), ErrorCode.SERVICE_ERROR.getMessage());
		}
	}

	@Test
	public void tansformToSystemObjectListTest() {
		List<Student> students = SystemBusinessTransformer.tansformToSystemObjectList(getStudentEntities(),
				Student.class);
		assertEquals(students, getStudents());
	}

	@Test
	public void tansformToSystemObjectSetTest() {
		Set<Course> courses = SystemBusinessTransformer.tansformToSystemObjectSet(getCourseEntities(), Course.class);
		assertEquals(courses.size(), getCourses().size());
		courses.addAll(getCourses());
		assertTrue(courses.size() == getCourses().size());
	}

	private List<StudentEntity> getStudentEntities() {
		List<StudentEntity> studentList = new ArrayList<>();
		StudentEntity se1 = new StudentEntity();
		se1.setId(1l);
		se1.setFirstName("Jon");
		se1.setLastName("Snow");

		StudentEntity se2 = new StudentEntity();
		se2.setId(2l);
		se2.setFirstName("Harvey");
		se2.setLastName("Specter");
		studentList.add(se1);
		studentList.add(se2);
		return studentList;
	}

	private Course getCourse() {
		Course c = new Course();
		c.setId(1l);
		c.setCourseName("Java and J2ee");
		return c;
	}

	private CourseEntity getCourseEntity() {
		CourseEntity c = new CourseEntity();
		c.setId(1l);
		c.setCourseName("Java and J2ee");
		return c;
	}

	private Set<CourseEntity> getCourseEntities() {
		Set<CourseEntity> courses = new HashSet<>();

		CourseEntity course1 = new CourseEntity();
		course1.setId(1l);
		course1.setCourseName("Java and J2ee");

		CourseEntity course2 = new CourseEntity();
		course2.setId(2l);
		course2.setCourseName("AWS Developer");
		courses.add(course1);
		courses.add(course2);
		return courses;
	}

	private List<Student> getStudents() {
		List<Student> studentList = new ArrayList<>();
		Student student1 = new Student();
		student1.setId(1l);
		student1.setFirstName("Jon");
		student1.setLastName("Snow");

		Student student2 = new Student();
		student2.setId(2l);
		student2.setFirstName("Harvey");
		student2.setLastName("Specter");
		studentList.add(student1);
		studentList.add(student2);
		return studentList;
	}

	private List<Course> getCourses() {
		List<Course> courses = new ArrayList<>();
		Course course1 = new Course();
		course1.setId(1l);
		course1.setCourseName("Java and J2ee");

		Course course2 = new Course();
		course2.setId(2l);
		course2.setCourseName("AWS Developer");
		courses.add(course1);
		courses.add(course2);
		return courses;
	}
}
