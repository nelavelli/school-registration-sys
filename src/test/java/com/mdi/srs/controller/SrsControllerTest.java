package com.mdi.srs.controller;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.fail;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.IntStream;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.mdi.srs.business.service.SrsService;
import com.mdi.srs.enums.Status;
import com.mdi.srs.exception.SRSProcessingException;
import com.mdi.srs.model.Course;
import com.mdi.srs.model.Response;
import com.mdi.srs.model.SearchFilter;
import com.mdi.srs.model.Student;

public class SrsControllerTest {

	@Mock
	private SrsService<Student> srsStudentService;

	@Mock
	private SrsService<Course> srsCourseService;

	@InjectMocks
	private SrsController srsController;

	MockHttpServletRequest request = new MockHttpServletRequest();

	@BeforeMethod
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
	}

	@Test
	public void deleteCourseTest() {
		Mockito.doNothing().when(srsCourseService).deregister(Mockito.anyLong());
		ResponseEntity<?> responseEntity = srsController.deleteCourse(1l);
		assertEquals(responseEntity.getStatusCodeValue(), HttpStatus.OK.value());
		assertEquals(responseEntity.getBody(), new Response(Status.SUCCESS, "Course deregistration is successful."));
		Mockito.verify(srsCourseService, Mockito.times(1)).deregister(Mockito.anyLong());
	}

	@Test
	public void deleteStudentTest() {
		Mockito.doNothing().when(srsStudentService).deregister(Mockito.anyLong());
		ResponseEntity<?> responseEntity = srsController.deleteStudent(1l);
		assertEquals(responseEntity.getStatusCodeValue(), HttpStatus.OK.value());
		assertEquals(responseEntity.getBody(), new Response(Status.SUCCESS, "Student deregistration is successful."));
		Mockito.verify(srsStudentService, Mockito.times(1)).deregister(Mockito.anyLong());
	}

	@Test
	public void enrollCourseForStudentsTest() {
		Long courseId = 1l;
		List<Long> studentIds = IntStream.range(2, 5).mapToLong(x -> x).collect(ArrayList<Long>::new, ArrayList::add,
				ArrayList::addAll);
		Mockito.doNothing().when(srsCourseService).enrollCourseForStudents(Mockito.anyLong(), Mockito.anyList());
		ResponseEntity<?> responseEntity = srsController.enrollCourseForStudents(courseId, studentIds);
		assertEquals(responseEntity.getStatusCodeValue(), HttpStatus.OK.value());
		assertEquals(responseEntity.getBody(),
				new Response(Status.SUCCESS, " Course " + courseId + "  enrollment for given Students is successful."));
		Mockito.verify(srsCourseService, Mockito.times(1)).enrollCourseForStudents(Mockito.anyLong(),
				Mockito.anyList());
	}

	@Test
	public void enrollCourseForStudentsNegativeTest() {
		Long courseId = 1l;
		List<Long> studentIds = IntStream.range(1, 52).mapToLong(x -> x).collect(ArrayList<Long>::new, ArrayList::add,
				ArrayList::addAll);
		try {
			srsController.enrollCourseForStudents(courseId, studentIds);
			fail("control should not reach to this line.");
		} catch (SRSProcessingException srspe) {
			assertEquals(srspe.getMessage(), "A course can have only 50 students registered.");
		}
		Mockito.verify(srsCourseService, Mockito.times(0)).enrollCourseForStudents(courseId, studentIds);
	}

	@Test
	public void enrollSutdentForCourseTest() {
		Long studentId = 1l;
		List<Long> courseIds = IntStream.range(2, 5).mapToLong(x -> x).collect(ArrayList<Long>::new, ArrayList::add,
				ArrayList::addAll);
		Mockito.doNothing().when(srsStudentService).enrollSutdentForCourses(Mockito.anyLong(), Mockito.anyList());
		ResponseEntity<?> responseEntity = srsController.enrollSutdentForCourses(studentId, courseIds);
		assertEquals(responseEntity.getStatusCodeValue(), HttpStatus.OK.value());
		assertEquals(responseEntity.getBody(),
				new Response(Status.SUCCESS, " Student " + studentId + " enrollment for given Courses is successful."));
		Mockito.verify(srsStudentService, Mockito.times(1)).enrollSutdentForCourses(Mockito.anyLong(),
				Mockito.anyList());
	}

	@Test
	public void enrollSutdentForCourseNegativeTest() {
		Long studentId = 1l;
		List<Long> courseIds = IntStream.range(1, 7).mapToLong(x -> x).collect(ArrayList<Long>::new, ArrayList::add,
				ArrayList::addAll);
		try {
			srsController.enrollSutdentForCourses(studentId, courseIds);
			fail("control should not reach to this line.");
		} catch (SRSProcessingException srspe) {
			assertEquals(srspe.getMessage(), "A student can have only 5 courses registered.");

		}
		Mockito.verify(srsStudentService, Mockito.times(0)).enrollSutdentForCourses(studentId, courseIds);
	}

	@Test
	public void findCourseTest() {
		Course course = getCourse();
		Mockito.when(srsCourseService.find(1l)).thenReturn(course);
		ResponseEntity<?> responseEntity = srsController.findCourse(1l);
		assertEquals(responseEntity.getStatusCodeValue(), HttpStatus.OK.value());
		assertEquals(responseEntity.getBody(), course);
		Mockito.verify(srsCourseService, Mockito.times(1)).find(1l);
	}

	@Test
	public void findCoursesTest() {
		List<Course> expected = getCourses();
		Mockito.when(srsCourseService.findAll()).thenReturn(expected);
		ResponseEntity<?> actual = srsController.findCourses();
		assertEquals(actual.getStatusCodeValue(), HttpStatus.OK.value());
		assertEquals(actual.getBody(), expected);
		Mockito.verify(srsCourseService, Mockito.times(1)).findAll();
	}

	@Test
	public void findStudentTest() {
		Student student = getStudent();
		Mockito.when(srsStudentService.find(1l)).thenReturn(student);
		ResponseEntity<?> responseEntity = srsController.findStudent(1l);
		assertEquals(responseEntity.getStatusCodeValue(), HttpStatus.OK.value());
		assertEquals(responseEntity.getBody(), student);
		Mockito.verify(srsStudentService, Mockito.times(1)).find(1l);
	}

	@Test
	public void findStudentsTest() throws Exception {
		List<Student> expected = getStudents();
		Mockito.when(srsStudentService.findAll()).thenReturn(expected);
		ResponseEntity<?> actual = srsController.findStudents();
		assertEquals(actual.getStatusCodeValue(), HttpStatus.OK.value());
		assertEquals(actual.getBody(), expected);
		Mockito.verify(srsStudentService, Mockito.times(1)).findAll();
	}

	@Test
	public void registerCourseTest() {

		Course inputCourse = new Course();
		inputCourse.setCourseName("Java and J2ee");

		Course expectedCourse = new Course();
		expectedCourse.setCourseName("Java and J2ee");
		expectedCourse.setId(1l);

		Mockito.when(srsCourseService.register(inputCourse)).thenReturn(expectedCourse);
		ResponseEntity<?> responseEntity = srsController.registerCourse(inputCourse);
		assertEquals(responseEntity.getStatusCodeValue(), HttpStatus.CREATED.value());
		assertEquals(responseEntity.getBody(), expectedCourse);
		Mockito.verify(srsCourseService, Mockito.times(1)).register(inputCourse);
	}

	@Test
	public void registerCoursesTest() {
		Set<Course> inputCourses = getRegCourses();
		List<Course> expCourses = getCourses();

		Mockito.when(srsCourseService.registerAll(inputCourses)).thenReturn(expCourses);
		ResponseEntity<?> responseEntity = srsController.registerCourses(inputCourses);
		assertEquals(responseEntity.getStatusCodeValue(), HttpStatus.CREATED.value());
		assertEquals(responseEntity.getBody(), expCourses);
		Mockito.verify(srsCourseService, Mockito.times(1)).registerAll(inputCourses);
	}

	@Test
	public void registerStudentTest() {

		Student inStudent = new Student();
		inStudent.setFirstName("Naresh");
		inStudent.setLastName("Nelavelli");

		Student expStudent = new Student();
		expStudent.setId(1l);
		expStudent.setFirstName("Naresh");
		expStudent.setLastName("Nelavelli");

		Mockito.when(srsStudentService.register(inStudent)).thenReturn(expStudent);
		ResponseEntity<?> responseEntity = srsController.registerStudent(inStudent);
		assertEquals(responseEntity.getStatusCodeValue(), HttpStatus.CREATED.value());
		assertEquals(responseEntity.getBody(), expStudent);
		Mockito.verify(srsStudentService, Mockito.times(1)).register(inStudent);
	}

	@Test
	public void registerStudentsTest() {

		Set<Student> inputStudents = getRegStudents();
		List<Student> expStudents = getStudents();

		Mockito.when(srsStudentService.registerAll(inputStudents)).thenReturn(expStudents);
		ResponseEntity<?> responseEntity = srsController.registerStudents(inputStudents);
		assertEquals(responseEntity.getStatusCodeValue(), HttpStatus.CREATED.value());
		assertEquals(responseEntity.getBody(), expStudents);
		Mockito.verify(srsStudentService, Mockito.times(1)).registerAll(inputStudents);
	}

	@Test
	public void srsFilterTest() {
		SearchFilter filter = new SearchFilter();
		filter.setFirstName("Naresh");
		Mockito.when(srsStudentService.searchSrsSystem(filter)).thenReturn(Arrays.asList());
		ResponseEntity<?> responseEntity = srsController.srsFilter(filter);
		assertEquals(responseEntity.getStatusCodeValue(), HttpStatus.OK.value());
		assertEquals(responseEntity.getBody(), Arrays.asList());
		Mockito.verify(srsStudentService, Mockito.times(1)).searchSrsSystem(filter);
	}

	@Test
	public void updateCourseTest() {
		Course inputCourse = new Course();
		inputCourse.setCourseName("Java and J2ee");

		Course expectedCourse = new Course();
		expectedCourse.setCourseName("Java and J2ee");
		expectedCourse.setId(1l);

		Mockito.when(srsCourseService.update(1l, inputCourse)).thenReturn(expectedCourse);
		ResponseEntity<?> responseEntity = srsController.updateCourse(1l, inputCourse);
		assertEquals(responseEntity.getStatusCodeValue(), HttpStatus.CREATED.value());
		assertEquals(responseEntity.getBody(), expectedCourse);
		Mockito.verify(srsCourseService, Mockito.times(1)).update(1l, inputCourse);
	}

	@Test
	public void updateStudentTest() {

		Student inStudent = new Student();
		inStudent.setFirstName("Naresh");
		inStudent.setLastName("Nelavelli");

		Student expStudent = new Student();
		expStudent.setId(1l);
		expStudent.setFirstName("Naresh");
		expStudent.setLastName("Nelavelli");

		Mockito.when(srsStudentService.update(1l, inStudent)).thenReturn(expStudent);
		ResponseEntity<?> responseEntity = srsController.updateStudent(1l, inStudent);
		assertEquals(responseEntity.getStatusCodeValue(), HttpStatus.CREATED.value());
		assertEquals(responseEntity.getBody(), expStudent);
		Mockito.verify(srsStudentService, Mockito.times(1)).update(1l, inStudent);
	}

	private Student getStudent() {
		Student s = new Student();
		s.setId(1l);
		s.setFirstName("Jon");
		s.setLastName("Snow");
		return s;
	}

	private Course getCourse() {
		Course c = new Course();
		c.setId(1l);
		c.setCourseName("Java and J2ee");
		return c;
	}

	private List<Student> getStudents() {
		List<Student> studentList = new ArrayList<>();
		Student student1 = new Student();
		student1.setId(1l);
		student1.setFirstName("Jon");
		student1.setLastName("Snow");

		Student student2 = new Student();
		student2.setId(2l);
		student2.setFirstName("Harvey");
		student2.setLastName("Specter");
		studentList.add(student1);
		studentList.add(student2);
		return studentList;
	}

	private List<Course> getCourses() {
		List<Course> courses = new ArrayList<>();
		Course course1 = new Course();
		course1.setId(1l);
		course1.setCourseName("Java and J2ee");

		Course course2 = new Course();
		course2.setId(2l);
		course2.setCourseName("AWS Developer");
		courses.add(course1);
		courses.add(course2);
		return courses;
	}

	private Set<Course> getRegCourses() {
		return new HashSet<Course>(getCourses());
	}

	private Set<Student> getRegStudents() {
		return new HashSet<>(getStudents());
	}

}
