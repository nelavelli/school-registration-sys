package com.mdi.srs;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.mdi.srs.enums.ErrorCode;
import com.mdi.srs.model.Course;
import com.mdi.srs.model.Response;
import com.mdi.srs.model.SearchFilter;
import com.mdi.srs.model.Student;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SchoolRegistrationSysApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("dev")
public class SchoolRegistrationSysApplicationTests {

	@LocalServerPort
	private int port;

	TestRestTemplate restTemplate = new TestRestTemplate();

	HttpHeaders headers = new HttpHeaders();

	private String createURLWithPort(String uri) {
		return "http://localhost:" + port + "/srs/api/v1/" + uri;
	}

	@Test
	public void testaddStudent() {
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Student> entity = new HttpEntity<Student>(getStudent(), headers);
		ResponseEntity<Student> response = restTemplate.exchange(createURLWithPort("student"), HttpMethod.POST, entity,
				Student.class);
		assertEquals(response.getStatusCodeValue(), 201);
		assertNotNull(response.getBody());
	}

	@Test
	public void testaddStudents() {
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<List<Student>> entity = new HttpEntity<List<Student>>(getStudents(), headers);
		ResponseEntity<StudentList> response = restTemplate.exchange(createURLWithPort("students"), HttpMethod.POST,
				entity, StudentList.class);
		assertEquals(response.getStatusCodeValue(), 201);
		assertNotNull(null);
	}

	@Test
	public void testRetrieveStudents() {
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<String>(null, headers);
		ResponseEntity<StudentList> response = restTemplate.exchange(createURLWithPort("students"), HttpMethod.GET,
				entity, StudentList.class);
		assertEquals(response.getStatusCodeValue(), 200);
		assertNotNull(response.getBody());
	}

	@Test
	public void testUpdateStudent() {

		// get anyone student from DB and update it.

		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<String>(null, headers);
		ResponseEntity<StudentList> responseList = restTemplate.exchange(createURLWithPort("students"), HttpMethod.GET,
				entity, StudentList.class);

		// update the object value
		Student sysStudent = responseList.getBody().getStudents().get(0);
		long sId = sysStudent.getId();
		sysStudent.setFirstName("fnameUpdated");
		sysStudent.setFirstName("lnameUpdated");
		sysStudent.setId(null);

		// send to system back
		HttpEntity<Student> updateEntity = new HttpEntity<Student>(sysStudent, headers);
		ResponseEntity<Student> response = restTemplate.exchange(createURLWithPort("students/" + sId), HttpMethod.POST,
				updateEntity, Student.class);
		assertEquals(response.getStatusCodeValue(), 201);
		assertNotNull(response.getBody());
		sysStudent.setId(sId);
		// compare updated one.
		assertEquals(sysStudent, response.getBody());
	}

	@Test
	public void testUpdateStudentNotAvaiable() {
		Student student = getStudent();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Student> updateEntity = new HttpEntity<Student>(student, headers);
		ResponseEntity<Response> response = restTemplate.exchange(createURLWithPort("students/2322"), HttpMethod.PUT,
				updateEntity, Response.class);
		assertEquals(response.getStatusCodeValue(), 400);
		assertEquals(response.getBody(), ErrorCode.DATA_NOT_FOUND);
	}

	@Test
	public void testDeleteStudent() {
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<String>(null, headers);
		ResponseEntity<Response> response = restTemplate.exchange(createURLWithPort("students/1"), HttpMethod.DELETE,
				entity, Response.class);
		assertEquals(response.getStatusCodeValue(), 200);
		assertEquals(response.getBody(), "Student deregistration is successful.");
	}

	@Test
	public void testDeleteStudentNotAvaiable() {
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<String>(null, headers);
		ResponseEntity<Response> response = restTemplate.exchange(createURLWithPort("students/11111"),
				HttpMethod.DELETE, entity, Response.class);
		assertEquals(response.getStatusCodeValue(), 400);
		assertEquals(response.getBody(), ErrorCode.DATA_NOT_FOUND);
	}

	@Test
	public void testRetrieveCourses() {
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<String>(null, headers);
		ResponseEntity<?> response = restTemplate.exchange(createURLWithPort("courses"), HttpMethod.GET, entity,
				CourseList.class);
		assertEquals(response.getStatusCodeValue(), 200);
		assertNotNull(response.getBody());
	}

	@Test
	public void testaddCourse() {
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Course> entity = new HttpEntity<Course>(getCourse(), headers);
		ResponseEntity<Course> response = restTemplate.exchange(createURLWithPort("course"), HttpMethod.POST, entity,
				Course.class);
		assertEquals(response.getStatusCodeValue(), 201);
		assertNotNull(response.getBody());
	}

	@Test
	public void testaddCourses() {
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<List<Course>> entity = new HttpEntity<List<Course>>(getCourses(), headers);
		ResponseEntity<CourseList> response = restTemplate.exchange(createURLWithPort("courses"), HttpMethod.POST,
				entity, CourseList.class);
		assertEquals(response.getStatusCodeValue(), 201);
		assertNotNull(response.getBody());
	}

	@Test
	public void testUpdateCourse() {

		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<String>(null, headers);
		ResponseEntity<CourseList> responseList = restTemplate.exchange(createURLWithPort("courses"), HttpMethod.GET,
				entity, CourseList.class);

		Course sysCourse = responseList.getBody().getCourses().get(0);
		long cId = sysCourse.getId();
		sysCourse.setCourseName("cnameUpdated");
		sysCourse.setId(null);

		HttpEntity<Course> updateEntity = new HttpEntity<Course>(sysCourse, headers);
		ResponseEntity<Course> response = restTemplate.exchange(createURLWithPort("courses/" + cId), HttpMethod.POST,
				updateEntity, Course.class);
		assertEquals(response.getStatusCodeValue(), 201);
		assertNotNull(response.getBody());
		sysCourse.setId(cId);
		assertEquals(sysCourse, response.getBody());
	}

	@Test
	public void testUpdateCourseNotAvaiable() {
		Course course = getCourse();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Course> entity = new HttpEntity<Course>(course, headers);
		ResponseEntity<Response> response = restTemplate.exchange(createURLWithPort("courses/2222"), HttpMethod.PUT,
				entity, Response.class);
		assertEquals(response.getStatusCodeValue(), 400);
		assertEquals(response.getBody(), ErrorCode.DATA_NOT_FOUND);
	}

	@Test
	public void testDeleteCourse() {
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<String>(null, headers);
		ResponseEntity<Response> response = restTemplate.exchange(createURLWithPort("courses/1"), HttpMethod.DELETE,
				entity, Response.class);
		assertEquals(response.getStatusCodeValue(), 200);
		assertEquals(response.getBody(), "Course deregistration is successful.");
	}

	@Test
	public void testDeleteCourseNotAvaiable() {
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<String>(null, headers);
		ResponseEntity<Response> response = restTemplate.exchange(createURLWithPort("courses/11111"), HttpMethod.DELETE,
				entity, Response.class);
		assertEquals(response.getStatusCodeValue(), 400);
		assertEquals(response.getBody(), ErrorCode.DATA_NOT_FOUND);
	}

	@Test
	public void testEnrollCourseForStudents() {
		headers.setContentType(MediaType.APPLICATION_JSON);
		List<Long> studentIds = IntStream.range(1, 3).mapToLong(x -> x).collect(ArrayList<Long>::new, ArrayList::add,
				ArrayList::addAll);
		HttpEntity<List<Long>> entity = new HttpEntity<List<Long>>(studentIds, headers);
		ResponseEntity<Response> response = restTemplate.exchange(createURLWithPort("/enroll/course/1"),
				HttpMethod.POST, entity, Response.class);
		assertEquals(response.getStatusCodeValue(), 200);
		assertEquals(response.getBody(), " Course 1  enrollment for given Students is successful.");
	}

	@Test
	public void testEnrollCourseForStudentsExceeds() {
		headers.setContentType(MediaType.APPLICATION_JSON);
		List<Long> studentIds = IntStream.range(1, 55).mapToLong(x -> x).collect(ArrayList<Long>::new, ArrayList::add,
				ArrayList::addAll);
		HttpEntity<List<Long>> entity = new HttpEntity<List<Long>>(studentIds, headers);
		ResponseEntity<Response> response = restTemplate.exchange(createURLWithPort("/enroll/course/1"),
				HttpMethod.POST, entity, Response.class);
		assertEquals(response.getStatusCodeValue(), 429);
		assertEquals(response.getBody(), ErrorCode.STUDENT_LIMIT_EXCEEDS);
	}

	@Test
	public void testEnrollSutdentForCourses() {
		headers.setContentType(MediaType.APPLICATION_JSON);
		List<Long> courseIds = IntStream.range(1, 3).mapToLong(x -> x).collect(ArrayList<Long>::new, ArrayList::add,
				ArrayList::addAll);
		HttpEntity<List<Long>> entity = new HttpEntity<List<Long>>(courseIds, headers);
		ResponseEntity<Response> response = restTemplate.exchange(createURLWithPort("/enroll/student/1"),
				HttpMethod.POST, entity, Response.class);
		assertEquals(response.getStatusCodeValue(), 200);
		assertEquals(response.getBody(), " Student 1 enrollment for given Courses is successful.");
	}

	@Test
	public void testEnrollSutdentForCoursesExceeds() {
		headers.setContentType(MediaType.APPLICATION_JSON);
		List<Long> courseIds = IntStream.range(1, 7).mapToLong(x -> x).collect(ArrayList<Long>::new, ArrayList::add,
				ArrayList::addAll);
		HttpEntity<List<Long>> entity = new HttpEntity<List<Long>>(courseIds, headers);
		ResponseEntity<Response> response = restTemplate.exchange(createURLWithPort("/enroll/student/1"),
				HttpMethod.POST, entity, Response.class);
		assertEquals(response.getStatusCodeValue(), 429);
		assertEquals(response.getBody(), ErrorCode.COURSE_LIMIT_EXCEEDS);
	}

	@Test
	public void testSearchFilter() {
		headers.setContentType(MediaType.APPLICATION_JSON);
		SearchFilter filter = new SearchFilter();
		filter.setFirstName("John");
		HttpEntity<SearchFilter> entity = new HttpEntity<SearchFilter>(filter, headers);
		ResponseEntity<StudentList> response = restTemplate.exchange(createURLWithPort("/admin/report"),
				HttpMethod.POST, entity, StudentList.class);
		assertEquals(response.getStatusCodeValue(), 200);
		assertNotNull(response.getBody());
	}

	@Test
	public void testSearchCourseFilter() {
		headers.setContentType(MediaType.APPLICATION_JSON);
		SearchFilter filter = new SearchFilter();
		filter.setCourseName("Machine Learning");
		HttpEntity<SearchFilter> entity = new HttpEntity<SearchFilter>(filter, headers);
		ResponseEntity<CourseList> response = restTemplate.exchange(createURLWithPort("/admin/report"), HttpMethod.POST,
				entity, CourseList.class);
		assertEquals(response.getStatusCodeValue(), 200);
		assertNotNull(response.getBody());
	}

	private Student getStudent() {
		Student s = new Student();
		s.setFirstName("Jon");
		s.setLastName("Snow");
		return s;
	}

	private Course getCourse() {
		Course c = new Course();
		c.setCourseName("Java and J2ee");
		return c;
	}

	private List<Student> getStudents() {
		List<Student> studentList = new ArrayList<>();
		Student student1 = new Student();
		student1.setId(1l);
		student1.setFirstName("Jon");
		student1.setLastName("Snow");

		Student student2 = new Student();
		student2.setId(2l);
		student2.setFirstName("Harvey");
		student2.setLastName("Specter");
		studentList.add(student1);
		studentList.add(student2);
		return studentList;
	}

	private List<Course> getCourses() {
		List<Course> courses = new ArrayList<>();
		Course course1 = new Course();
		course1.setCourseName("Java and J2ee");

		Course course2 = new Course();
		course2.setCourseName("AWS Developer");
		courses.add(course1);
		courses.add(course2);
		return courses;
	}

	private class StudentList {

		private List<Student> students = new ArrayList<>();

		public List<Student> getStudents() {
			return students;
		}

		public void setStudents(List<Student> students) {
			this.students = students;
		}

		@Override
		public String toString() {
			return "StudentList [students=" + students + "]";
		}
	}

	private class CourseList {

		private List<Course> courses = new ArrayList<>();

		public List<Course> getCourses() {
			return courses;
		}

		public void setCourses(List<Course> courses) {
			this.courses = courses;
		}

		@Override
		public String toString() {
			return "CourseList [courses=" + courses + "]";
		}
	}
}
