--TBL_STUDENTS data
INSERT INTO TBL_STUDENT(ST_ID, ST_FNM, ST_LNM, CRT_TS, CHG_TS) VALUES (1, 'Naresh', 'Nelavelli', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO TBL_STUDENT(ST_ID, ST_FNM, ST_LNM, CRT_TS, CHG_TS) VALUES (2, 'Narendra', 'Nelavelli', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);

--TBL_COURSES Data
INSERT INTO TBL_COURSE(CRS_ID, CRS_NM, CRT_TS, CHG_TS) VALUES (1, 'Java and J2ee', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO TBL_COURSE(CRS_ID, CRS_NM, CRT_TS, CHG_TS) VALUES (2, 'Machine Learning', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);

--TBL_COURSE_ENRLMT
INSERT INTO TBL_COURSE_ENRLMT(ENRLMT_ID, ST_ID, CRS_ID, CRT_TS, CHG_TS) values (1, 1, 1, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO TBL_COURSE_ENRLMT(ENRLMT_ID, ST_ID, CRS_ID, CRT_TS, CHG_TS) values (2, 1, 2, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO TBL_COURSE_ENRLMT(ENRLMT_ID, ST_ID, CRS_ID, CRT_TS, CHG_TS) values (3, 2, 2, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);

--INSERT INTO TBL_COURSE_ENRLMT(ST_ID, CRS_ID) values ( 1, 1);
--INSERT INTO TBL_COURSE_ENRLMT(ST_ID, CRS_ID) values ( 1, 2);
--INSERT INTO TBL_COURSE_ENRLMT(ST_ID, CRS_ID) values ( 2, 2);

