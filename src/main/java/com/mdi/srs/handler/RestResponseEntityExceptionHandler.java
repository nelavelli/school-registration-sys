package com.mdi.srs.handler;

import java.lang.invoke.MethodHandles;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.mdi.srs.model.InvalidField;
import com.mdi.srs.model.Response;
import com.mdi.srs.enums.ErrorCode;
import com.mdi.srs.enums.Status;
import com.mdi.srs.exception.SRSProcessingException;

@RestControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	@ExceptionHandler(value = { SRSProcessingException.class })
	public ResponseEntity<Object> handleException(SRSProcessingException srsEx, WebRequest request) {
		return new ResponseEntity<>(new Response(Status.FAILED, srsEx.getLocalizedMessage()), srsEx.getHttpsStatus());
	}

	@ExceptionHandler(value = ConstraintViolationException.class)
	public ResponseEntity<Object> handleConstraintViolationException(ConstraintViolationException exception) {
		List<InvalidField> invalidFields = exception.getConstraintViolations().stream()
				.map(cv -> new InvalidField(cv.getPropertyPath().toString(), cv.getMessage(),
						cv.getInvalidValue()))
				.collect(Collectors.toList());
		return new ResponseEntity<>(new Response(Status.FAILED, "Input Validation Error", invalidFields),
				HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(Exception.class)
	public ResponseEntity<Object> handle(Exception ex, HttpServletRequest request, HttpServletResponse response) {
		return new ResponseEntity<>(new Response(Status.FAILED, ErrorCode.SYSTEM_ERROR.getMessage()),
				HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@Override
	public ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers,
			HttpStatus status, WebRequest request) {
		Map<String, String> errors = new HashMap<>();
		ex.getBindingResult().getAllErrors().forEach((error) -> {
			String fieldName = ((FieldError) error).getField();
			String errorMessage = error.getDefaultMessage();
			errors.put(fieldName, errorMessage);
		});

		return new ResponseEntity<>(new Response(Status.FAILED, "Required field(s) missing", errors),
				HttpStatus.BAD_REQUEST);
	}

	@Override
	protected ResponseEntity<Object> handleHttpMediaTypeNotSupported(HttpMediaTypeNotSupportedException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {

		if (request.getParameterMap().isEmpty()) {
			return new ResponseEntity<>(
					new Response(Status.FAILED, "Empty Request Payload - Please check the API contract once."), headers,
					HttpStatus.BAD_REQUEST);

		}
		Map<String, String> errors = new HashMap<>();
		errors.put("Content-Type", "Unsupported content type: " + ex.getContentType());
		errors.put("Supported content types: ", MediaType.toString(ex.getSupportedMediaTypes()));
		return new ResponseEntity<>(new Response(Status.FAILED,
				"Invalid Content Type is passed - Please check the API contract once.", errors), headers, status);
	}

	@Override
	protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		Throwable mostSpecificCause = ex.getMostSpecificCause();
		String errorMessage = ex.getMessage();
		if (mostSpecificCause != null) {
			errorMessage = mostSpecificCause.getMessage();
			errorMessage = errorMessage.substring(0, errorMessage.indexOf(":"));
		}
		return new ResponseEntity<>(new Response(Status.FAILED, errorMessage), headers, status);
	}
}
