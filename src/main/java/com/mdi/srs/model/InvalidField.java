package com.mdi.srs.model;

import java.io.Serializable;

public class InvalidField implements Serializable {

	private static final long serialVersionUID = -8882995128379421221L;

	private String name;

	private String message;

	private Object value;

	public InvalidField(String name, String message, Object value) {
		this.name = name;
		this.message = message;
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}
}
