package com.mdi.srs.model;

import java.io.Serializable;

public class SearchFilter implements Serializable {

	private static final long serialVersionUID = -6305485436476079535L;
	
	private Long studentId;
	
	private Long courseId;
	
	private String firstName;
	
	private String lastName;
	
	private String courseName;
	
	private boolean courseWithoutStudent;
	
	private boolean studentWithoutCourse;

	public Long getStudentId() {
		return studentId;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	public Long getCourseId() {
		return courseId;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public boolean isCourseWithoutStudent() {
		return courseWithoutStudent;
	}

	public void setCourseWithoutStudent(boolean courseWithoutStudent) {
		this.courseWithoutStudent = courseWithoutStudent;
	}

	public boolean isStudentWithoutCourse() {
		return studentWithoutCourse;
	}

	public void setStudentWithoutCourse(boolean studentWithoutCourse) {
		this.studentWithoutCourse = studentWithoutCourse;
	}

	@Override
	public String toString() {
		return "SearchFilter [studentId=" + studentId + ", courseId=" + courseId + ", firstName=" + firstName
				+ ", lastName=" + lastName + ", courseName=" + courseName + ", courseWithoutStudent="
				+ courseWithoutStudent + ", studentWithoutCourse=" + studentWithoutCourse + "]";
	}
}
