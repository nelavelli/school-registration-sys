package com.mdi.srs.model;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.mdi.srs.enums.Status;

@JsonInclude(Include.NON_NULL)
public class Response implements Serializable {

	private static final long serialVersionUID = 8013681827002598741L;

	private Status status;

	private String message;

	private Map<String, String> fields;

	private List<InvalidField> missingRequiredFields;

	public Response(Status status, String message) {
		this.status = status;
		this.message = message;
	}

	public Response(Status status, String message, Map<String, String> fields) {
		this.status = status;
		this.message = message;
		this.fields = fields;
	}

	public Response(Status status, String message, List<InvalidField> missingRequiredFields) {
		this.status = status;
		this.message = message;
		this.setMissingRequiredFields(missingRequiredFields);
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Map<String, String> getFields() {
		return fields;
	}

	public void setFields(Map<String, String> fields) {
		this.fields = fields;
	}

	public List<InvalidField> getMissingRequiredFields() {
		return missingRequiredFields;
	}

	public void setMissingRequiredFields(List<InvalidField> missingRequiredFields) {
		this.missingRequiredFields = missingRequiredFields;
	}

	@Override
	public String toString() {
		return "Response [status=" + status + ", message=" + message + ", fields=" + fields + ", missingRequiredFields="
				+ missingRequiredFields + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fields == null) ? 0 : fields.hashCode());
		result = prime * result + ((message == null) ? 0 : message.hashCode());
		result = prime * result + ((missingRequiredFields == null) ? 0 : missingRequiredFields.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Response other = (Response) obj;
		if (fields == null) {
			if (other.fields != null)
				return false;
		} else if (!fields.equals(other.fields))
			return false;
		if (message == null) {
			if (other.message != null)
				return false;
		} else if (!message.equals(other.message))
			return false;
		if (missingRequiredFields == null) {
			if (other.missingRequiredFields != null)
				return false;
		} else if (!missingRequiredFields.equals(other.missingRequiredFields))
			return false;
		if (status != other.status)
			return false;
		return true;
	}

}
