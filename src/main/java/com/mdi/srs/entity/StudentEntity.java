package com.mdi.srs.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "TBL_STUDENT")
public class StudentEntity extends RecordTimeStamp {

	private static final long serialVersionUID = -4929605751107396097L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ST_ID", nullable = false)
	private Long id;

	@Column(name = "ST_FNM", nullable = false)
	private String firstName;

	@Column(name = "ST_LNM", nullable = false)
	private String lastName;

	@ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.DETACH,CascadeType.MERGE,CascadeType.REFRESH}, fetch = FetchType.LAZY)
	@JoinTable(name = "TBL_COURSE_ENRLMT", joinColumns = { @JoinColumn(name = "ST_ID") }, inverseJoinColumns = {
			@JoinColumn(name = "CRS_ID") })
	@JsonIgnore
	private Set<CourseEntity> courseSet = new HashSet<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Set<CourseEntity> getCourseSet() {
		return courseSet;
	}

	public void setCourseSet(Set<CourseEntity> courseSet) {
		this.courseSet = courseSet;
	}

	@Override
	public String toString() {
		return "StudentEntity [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", courseSet="
				+ courseSet + "]";
	}

}
