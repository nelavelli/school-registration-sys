package com.mdi.srs.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "TBL_COURSE")
public class CourseEntity extends RecordTimeStamp {

	private static final long serialVersionUID = -2412038051114699933L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "CRS_ID", nullable = false)
	private Long id;

	@Column(name = "CRS_NM", nullable = false)
	private String courseName;

	@ManyToMany(mappedBy = "courseSet", cascade = {CascadeType.PERSIST, CascadeType.DETACH,CascadeType.MERGE,CascadeType.REFRESH}, fetch = FetchType.LAZY)
	private Set<StudentEntity> studentSet = new HashSet<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public Set<StudentEntity> getStudentSet() {
		return studentSet;
	}

	public void setStudentSet(Set<StudentEntity> studentSet) {
		this.studentSet = studentSet;
	}

	@Override
	public String toString() {
		return "CourseEntity [id=" + id + ", courseName=" + courseName + ", studentSet=" + studentSet + "]";
	}

}
