package com.mdi.srs.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;

@MappedSuperclass
public class RecordTimeStamp implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name = "CRT_TS")
	@Temporal(TemporalType.TIMESTAMP)
	@JsonBackReference
	private Date createdTimestamp;

	@Column(name = "CHG_TS")
	@Temporal(TemporalType.TIMESTAMP)
	@JsonBackReference
	private Date updatedTime;

	public Date getCreatedTime() {
		return createdTimestamp;
	}

	public void setCreatedTime(Date createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public Date getUpdatedTime() {
		return updatedTime;
	}

	public void setUpdatedTime(Date updatedTime) {
		this.updatedTime = updatedTime;
	}

	@PrePersist
	public void creationDate() {
		this.setCreatedTime(new Date());
		this.setUpdatedTime(new Date());
	}

	@PreUpdate
	public void lastModified() {
		this.setUpdatedTime(new Date());
	}

}