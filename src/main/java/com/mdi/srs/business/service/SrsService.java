package com.mdi.srs.business.service;

import java.util.List;
import java.util.Set;

import com.mdi.srs.model.SearchFilter;

public interface SrsService<T> {

	T register(T e);

	T update(Long id, T e);

	void deregister(Long id);

	T find(Long id);

	List<T> findAll();

	List<T> registerAll(Set<T> set);

	void enrollSutdentForCourses(Long studentId, List<Long> cIds);

	void enrollCourseForStudents(Long courseId, List<Long> sIds);

	List<?> searchSrsSystem(SearchFilter filter);

}
