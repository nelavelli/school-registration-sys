package com.mdi.srs.business.service.impl;

import java.lang.invoke.MethodHandles;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import com.mdi.srs.exception.SRSProcessingException;
import com.mdi.srs.model.Course;
import com.mdi.srs.model.SearchFilter;
import com.mdi.srs.model.Student;
import com.mdi.srs.repository.SrsBaseRepository;
import com.mdi.srs.business.service.SrsService;
import com.mdi.srs.entity.CourseEntity;
import com.mdi.srs.entity.StudentEntity;
import com.mdi.srs.enums.ErrorCode;

import static com.mdi.srs.transformer.SystemBusinessTransformer.tansformToSystemObjectList;
import static com.mdi.srs.transformer.SystemBusinessTransformer.tansformToSystemObjectSet;
import static com.mdi.srs.transformer.SystemBusinessTransformer.tansformToSystemObject;
import static com.mdi.srs.constants.SrsServicesConstants.MAX_COURSES_PER_STUDENT;
import static com.mdi.srs.constants.SrsServicesConstants.MAX_STUDENTS_PER_COURSE;
import static com.mdi.srs.repository.spec.SrsSpecificationUtil.bySrsFilter;

@Transactional
public class SrsServiceImpl<T, E, S> implements SrsService<T> {

	private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	private SrsBaseRepository<E, Long> repository;

	private SrsBaseRepository<S, Long> secondRepository;

	private Class<T> pojoClz;

	private Class<E> entityClz;

	public SrsServiceImpl(SrsBaseRepository<E, Long> repository, SrsBaseRepository<S, Long> secondRepository,
			Class<T> pojoClz, Class<E> entityClz) {
		this.repository = repository;
		this.secondRepository = secondRepository;
		this.pojoClz = pojoClz;
		this.entityClz = entityClz;
	}

	private E findById(Long id) {
		Optional<E> e = repository.findById(id);
		if (!e.isPresent()) {
			LOGGER.warn("System Error, Registered Student Id {} not found in DB." + id);
			throw new SRSProcessingException(ErrorCode.DATA_NOT_FOUND);
		}
		return e.get();
	}

	@Override
	public T find(Long id) throws SRSProcessingException {
		try {
			return tansformToSystemObject(findById(id), pojoClz);
		} catch (SRSProcessingException srspEx) {
			LOGGER.error("error while transfoming obj - find", srspEx);
			throw srspEx;
		}
	}

	@Override
	public List<T> findAll() {
		try {
			List<E> list = repository.findAll();
			return tansformToSystemObjectList(list, pojoClz);
		} catch (SRSProcessingException srspEx) {
			LOGGER.error("error while transfoming obj - findAll", srspEx);
			throw srspEx;
		}
	}

	@Override
	public T register(T o) {
		try {
			E e = tansformToSystemObject(o, entityClz);
			e = repository.save(e);
			return tansformToSystemObject(e, pojoClz);
		} catch (Exception ex) {
			LOGGER.error("error while peforming registerAll action ", ex);
			throw new SRSProcessingException(ErrorCode.SYSTEM_ERROR, ex);
		}
	}

	@Override
	public List<T> registerAll(Set<T> objs) {
		try {
			Set<E> objSet = tansformToSystemObjectSet(objs, entityClz);
			List<E> list = repository.saveAll(objSet);
			return tansformToSystemObjectList(list, pojoClz);
		} catch (Exception ex) {
			LOGGER.error("error while peforming registerAll action ", ex);
			throw new SRSProcessingException(ErrorCode.SYSTEM_ERROR, ex);
		}
	}

	@Override
	public T update(Long id, T o) {
		try {
			findById(id);
			return this.register(o);
		} catch (SRSProcessingException srspex) {
			throw srspex;
		}
	}

	@Override
	public void deregister(Long id) {
		try {
			findById(id);
			repository.deleteById(id);
		} catch (SRSProcessingException srspEx) {
			throw srspEx;
		}
	}

	@Override
	public List<?> searchSrsSystem(SearchFilter filter) {
		try {
			return (Objects.nonNull(filter) && filter.isCourseWithoutStudent()) ? searchSrsCourseSystem(filter)
					: searchSrsStudentSystem(filter);
		} catch (SRSProcessingException srsEx) {
			throw srsEx;
		}
	}

	@SuppressWarnings("unchecked")
	private List<?> searchSrsStudentSystem(SearchFilter filter) {
		try {
			List<StudentEntity> studentList = (List<StudentEntity>) repository.findAll(bySrsFilter(filter));

			return studentList.stream().map(studentEntity -> {
				Set<Course> courses = studentEntity.getCourseSet().stream()
						.map(courseEntity -> tansformToSystemObject(courseEntity, Course.class))
						.collect(Collectors.toSet());
				Student student = tansformToSystemObject(studentEntity, Student.class);
				student.setCourses(courses);
				return student;
			}).collect(Collectors.toList());
		} catch (Exception ex) {
			LOGGER.error("Error while  Searching for student data. ", ex);
			throw new SRSProcessingException(ErrorCode.SYSTEM_ERROR, ex);
		}
	}

	@SuppressWarnings("unchecked")
	private List<?> searchSrsCourseSystem(SearchFilter filter) {
		try {
			List<CourseEntity> courseList = (List<CourseEntity>) secondRepository.findAll(bySrsFilter(filter));
			return tansformToSystemObjectList(courseList, Course.class);
		} catch (Exception ex) {
			LOGGER.error("Error while Searching for Course data. ", ex);
			throw new SRSProcessingException(ErrorCode.SYSTEM_ERROR, ex);
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public void enrollSutdentForCourses(Long sId, List<Long> cIds) {

		try {

			Optional<StudentEntity> e = (Optional<StudentEntity>) repository.findById(sId);
			if (!e.isPresent()) {
				LOGGER.warn("System Error, Registered Student Id {} not found in DB." + sId);
				throw new SRSProcessingException(ErrorCode.DATA_NOT_FOUND);
			}
			StudentEntity studentEntity = e.get();

			// 1. find the existing courses, remove the common ones in new request and old
			// data if they are more than 5 return error.
			// assumption is that we do not need to show if the courses are already
			// registered.

			List<Long> existingCIds = studentEntity.getCourseSet().stream().map(ce -> ce.getId())
					.collect(Collectors.toList());

			cIds.removeAll(existingCIds);

			if (Math.addExact(cIds.size(), existingCIds.size()) > MAX_COURSES_PER_STUDENT)
				throw new SRSProcessingException(ErrorCode.COURSE_LIMIT_EXCEEDS);

			List<CourseEntity> courseEntities = (List<CourseEntity>) secondRepository.findAllById(cIds);

			List<Long> systemCIds = courseEntities.stream().map(ce -> ce.getId()).collect(Collectors.toList());

			// 2. find if any of the new courseID is not available in the system one.

			cIds.removeAll(systemCIds);

			if (cIds.size() > 0) {
				LOGGER.debug("Course Id(s) {} Not found in the data base, hence throwing ex.", cIds);
				throw new SRSProcessingException(ErrorCode.DATA_NOT_FOUND,
						"Course Id(s) " + cIds + " Not found in the System");
			}

			studentEntity.getCourseSet().addAll(courseEntities);
		} catch (SRSProcessingException srspEx) {
			throw srspEx;
		} catch (Exception ex) {
			LOGGER.error("Error while enrolling Sutdent For Courses ", ex);
			throw new SRSProcessingException(ErrorCode.SYSTEM_ERROR, ex);
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public void enrollCourseForStudents(Long cId, List<Long> sIds) {

		try {

			Optional<CourseEntity> e = (Optional<CourseEntity>) repository.findById(cId);
			if (!e.isPresent()) {
				LOGGER.debug("System Error, Registered course Id {} not found in DB." + cId);
				throw new SRSProcessingException(ErrorCode.DATA_NOT_FOUND);
			}
			CourseEntity courseEntity = e.get();

			// 1. find the existing Students, remove the common ones in new request and old
			// data if they are more than 50 return error.
			// assumption is that we do not need to show if the students are already
			// registered.

			List<Long> existingSIds = courseEntity.getStudentSet().stream().map(ce -> ce.getId())
					.collect(Collectors.toList());
			sIds.removeAll(existingSIds);

			if (Math.addExact(sIds.size(), existingSIds.size()) > MAX_STUDENTS_PER_COURSE) {
				throw new SRSProcessingException(ErrorCode.STUDENT_LIMIT_EXCEEDS);
			}

			List<StudentEntity> studentEntities = (List<StudentEntity>) secondRepository.findAllById(sIds);

			List<Long> systemSIds = studentEntities.stream().map(se -> se.getId()).collect(Collectors.toList());

			// 2. find if any of the new student Id is not available in the system one.

			sIds.removeAll(systemSIds);

			if (sIds.size() > 0) {
				LOGGER.debug("Student Id(s) {} Not found in the data base, hence throwing ex.", sIds);
				throw new SRSProcessingException(ErrorCode.DATA_NOT_FOUND,
						"Student Id(s) " + sIds + " Not found in the System.");
			}

			courseEntity.getStudentSet().addAll(studentEntities.stream().map(se -> {
				se.getCourseSet().add(courseEntity);
				return se;
			}).collect(Collectors.toSet()));
		} catch (SRSProcessingException srspEx) {
			throw srspEx;
		} catch (Exception ex) {
			LOGGER.error("Error while enroll Course For Students ", ex);
			throw new SRSProcessingException(ErrorCode.SYSTEM_ERROR, ex);
		}
	}
}
