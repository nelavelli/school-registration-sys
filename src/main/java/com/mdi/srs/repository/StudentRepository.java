package com.mdi.srs.repository;

import org.springframework.stereotype.Repository;

import com.mdi.srs.entity.StudentEntity;

@Repository
public interface StudentRepository extends SrsBaseRepository<StudentEntity, Long> {

}