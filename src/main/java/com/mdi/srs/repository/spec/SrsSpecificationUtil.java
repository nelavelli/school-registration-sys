package com.mdi.srs.repository.spec;

import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;

import com.mdi.srs.model.SearchFilter;

public class SrsSpecificationUtil {

	private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	private static String STUDENTS = "studentSet";

	private static String ID = "id";

	private static String FIRST_NAME = "firstName";

	private static String LAST_NAME = "lastName";

	private static String COURSES = "courseSet";

	private static String COURSE_NAME = "courseName";

	private static String PERCENTAILE = "%";

	public static <T> Specification<T> bySrsFilter(SearchFilter filter) {

		return (Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) -> {

			List<Predicate> predicates = new ArrayList<>();

			if (filter.isCourseWithoutStudent()) {
				if (!StringUtils.isEmpty(filter.getCourseName())) {
					predicates
							.add(criteriaBuilder.like(root.get(COURSE_NAME), likePlaceHolder(filter.getCourseName())));
				}

				if (!StringUtils.isEmpty(filter.getCourseId())) {
					predicates.add(criteriaBuilder.equal(root.get(ID), filter.getCourseId()));
				}

				predicates.add(criteriaBuilder.isEmpty(root.get(STUDENTS)));
				return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
			}

			if (!StringUtils.isEmpty(filter.getStudentId())) {
				predicates.add(criteriaBuilder.equal(root.<String>get(ID), filter.getStudentId()));
			}

			if (!StringUtils.isEmpty(filter.getFirstName())) {
				predicates.add(
						criteriaBuilder.like(root.<String>get(FIRST_NAME), likePlaceHolder(filter.getFirstName())));
			}

			if (!StringUtils.isEmpty(filter.getLastName())) {
				predicates
						.add(criteriaBuilder.like(root.<String>get(LAST_NAME), likePlaceHolder(filter.getLastName())));
			}

			if (!StringUtils.isEmpty(filter.getCourseName())) {
				predicates.add(criteriaBuilder.like(root.join(COURSES).get(COURSE_NAME),
						likePlaceHolder(filter.getCourseName())));
			}

			if (!StringUtils.isEmpty(filter.getCourseId())) {
				predicates.add(criteriaBuilder.equal(root.join(COURSES).get(ID), filter.getCourseId()));
			}

			if (filter.isStudentWithoutCourse()) {
				predicates.add(criteriaBuilder.isEmpty(root.get(COURSES)));
			}

			if (predicates.isEmpty()) {
				return criteriaBuilder.conjunction();
			}

			return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
		};
	}

	private static String likePlaceHolder(String value) {

		return (PERCENTAILE + value + PERCENTAILE);
	}

	public static <E, T> Specification<T> findByIds(List<Long> ids) {

		return (Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) -> {

			if (Objects.isNull(ids)) {
				return criteriaBuilder.conjunction();
			}

			List<Predicate> predicates = new ArrayList<>();
			predicates.add(criteriaBuilder.in(root.get("id")).value(ids));

			return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
		};
	}
}
