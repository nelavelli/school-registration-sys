package com.mdi.srs.repository;

import org.springframework.stereotype.Repository;

import com.mdi.srs.entity.CourseEntity;

@Repository
public interface CourseRepository extends SrsBaseRepository<CourseEntity, Long> {

}
