package com.mdi.srs.constants;

public class SrsServicesConstants {

	public static final String VERSION = "/v1";

	public static final String STUDENT_API = "/student";

	public static final String STUDENTS_API = "/students";

	public static final String STUDENT_ID_API = "/students/{studentId}";

	public static final String ENROLL_COURSE_API = "/enroll/student/{studentId}";

	public static final String ENROLL_STUDENT_API = "/enroll/course/{courseId}";

	public static final String COURSE_API = "/course";

	public static final String COURSES_API = "/courses";

	public static final String COURSE_ID_API = "/courses/{courseId}";

	public static final String REPORT_API = "/admin/report";

	public static final int MAX_STUDENTS_PER_COURSE = 50;

	public static final int MAX_COURSES_PER_STUDENT = 5;

}
