package com.mdi.srs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import com.mdi.srs.business.service.SrsService;
import com.mdi.srs.business.service.impl.SrsServiceImpl;
import com.mdi.srs.entity.CourseEntity;
import com.mdi.srs.entity.StudentEntity;
import com.mdi.srs.model.Course;
import com.mdi.srs.model.Student;
import com.mdi.srs.repository.CourseRepository;
import com.mdi.srs.repository.StudentRepository;

@SpringBootApplication(scanBasePackages = "com.mdi.srs")
@EnableJpaRepositories(basePackages = "com.mdi.srs.repository")
@EnableTransactionManagement
public class SchoolRegistrationSysApplication {

	public static void main(String[] args) {
		SpringApplication.run(SchoolRegistrationSysApplication.class, args);
	}

	@Bean(name = "srsStudentService")
	public SrsService<Student> getSrsStudentService(@Autowired StudentRepository studentRepository,
			@Autowired CourseRepository courseRepository) {
		return new SrsServiceImpl<Student, StudentEntity, CourseEntity>(studentRepository, courseRepository,
				Student.class, StudentEntity.class);
	}

	@Bean(name = "srsCourseService")
	public SrsService<Course> getSrsCourseService(@Autowired CourseRepository courseRepository,
			@Autowired StudentRepository studentRepository) {
		return new SrsServiceImpl<Course, CourseEntity, StudentEntity>(courseRepository, studentRepository,
				Course.class, CourseEntity.class);
	}

	@Bean
	public MessageSource messageSource() {
		ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
		messageSource.setBasename("classpath:messages");
		messageSource.setDefaultEncoding("UTF-8");
		return messageSource;
	}

	@Bean
	public LocalValidatorFactoryBean getValidator() {
		LocalValidatorFactoryBean bean = new LocalValidatorFactoryBean();
		bean.setValidationMessageSource(messageSource());
		return bean;
	}
}
