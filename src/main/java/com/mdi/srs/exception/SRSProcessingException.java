package com.mdi.srs.exception;

import java.lang.invoke.MethodHandles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

import com.mdi.srs.enums.ErrorCode;

public class SRSProcessingException extends RuntimeException {
	
	 private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	private static final long serialVersionUID = -58739879323013824L;
	
	private HttpStatus httpsStatus;

	public SRSProcessingException() {
		super();
	}

	public SRSProcessingException(ErrorCode errorMessages) {
		super(errorMessages.getMessage());
		this.httpsStatus = errorMessages.getStatus();
	}
	
	public SRSProcessingException(ErrorCode errorMessages, String message) {
		super(message);
		this.httpsStatus = errorMessages.getStatus();
	}
	
	public SRSProcessingException(HttpStatus httpsStatus, ErrorCode errorMessages) {
		super(errorMessages.getMessage());
		this.httpsStatus = errorMessages.getStatus();
	}

	public SRSProcessingException(String message) {
		super(message);
	}

	public SRSProcessingException(ErrorCode ErrorMessages, Throwable cause) {
		super(ErrorMessages.getMessage(), cause);
	}

	public SRSProcessingException(String message, Throwable cause) {
		super(message, cause);
	}

	public SRSProcessingException(Throwable cause) {
		super(cause);
	}

	public HttpStatus getHttpsStatus() {
		return httpsStatus;
	}

}
