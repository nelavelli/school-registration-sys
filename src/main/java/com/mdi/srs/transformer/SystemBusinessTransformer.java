package com.mdi.srs.transformer;

import java.lang.invoke.MethodHandles;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;

import com.mdi.srs.enums.ErrorCode;
import com.mdi.srs.exception.SRSProcessingException;

public class SystemBusinessTransformer {

	private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	public static <E, T> T tansformToSystemObject(E src, Class<T> targetClz) {
		try {
			T target = (T) targetClz.getDeclaredConstructor().newInstance();
			// supposed to use getDeclaredConstructor().newInstance() but only supports from
			// java 9, so keeping with the deprecated method.
			BeanUtils.copyProperties(src, target);
			return target;
		} catch (Exception ex) {
			LOGGER.error("Dynamic object creation failed in the system.", ex);
			throw new SRSProcessingException(ErrorCode.SERVICE_ERROR);
		}

	}

	public static <E, T> List<T> tansformToSystemObjectList(List<E> srcList, Class<T> clz) {
		return srcList.stream().map(src -> tansformToSystemObject(src, clz)).collect(Collectors.toList());
	}

	public static <E, T> Set<T> tansformToSystemObjectSet(Set<E> srcList, Class<T> clz) {
		return srcList.stream().map(src -> tansformToSystemObject(src, clz)).collect(Collectors.toSet());
	}

}
