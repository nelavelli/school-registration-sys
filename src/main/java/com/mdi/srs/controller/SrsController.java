package com.mdi.srs.controller;

import java.lang.invoke.MethodHandles;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mdi.srs.business.service.SrsService;
import com.mdi.srs.enums.ErrorCode;
import com.mdi.srs.enums.Status;
import com.mdi.srs.exception.SRSProcessingException;
import com.mdi.srs.model.Course;
import com.mdi.srs.model.Response;
import com.mdi.srs.model.SearchFilter;
import com.mdi.srs.model.Student;

import static com.mdi.srs.constants.SrsServicesConstants.COURSES_API;
import static com.mdi.srs.constants.SrsServicesConstants.COURSE_API;
import static com.mdi.srs.constants.SrsServicesConstants.COURSE_ID_API;
import static com.mdi.srs.constants.SrsServicesConstants.ENROLL_COURSE_API;
import static com.mdi.srs.constants.SrsServicesConstants.ENROLL_STUDENT_API;
import static com.mdi.srs.constants.SrsServicesConstants.REPORT_API;
import static com.mdi.srs.constants.SrsServicesConstants.STUDENTS_API;
import static com.mdi.srs.constants.SrsServicesConstants.STUDENT_API;
import static com.mdi.srs.constants.SrsServicesConstants.STUDENT_ID_API;
import static com.mdi.srs.constants.SrsServicesConstants.VERSION;
import static com.mdi.srs.constants.SrsServicesConstants.MAX_COURSES_PER_STUDENT;
import static com.mdi.srs.constants.SrsServicesConstants.MAX_STUDENTS_PER_COURSE;
import static com.monitorjbl.json.JsonView.with;
import static com.monitorjbl.json.Match.match;

@RestController
@Validated
@RequestMapping(path = VERSION, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class SrsController {

	private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	@Autowired
	@Qualifier("srsStudentService")
	private SrsService<Student> srsStudentService;

	@Autowired
	@Qualifier("srsCourseService")
	private SrsService<Course> srsCourseService;

	@GetMapping(STUDENT_ID_API)
	public ResponseEntity<?> findStudent(@NotNull(message = "{studentId.notEmtpy}") @PathVariable Long studentId) {
		Student student = srsStudentService.find(studentId);
		return new ResponseEntity<>(student, HttpStatus.OK);
	}

	@PostMapping(STUDENT_API)
	public ResponseEntity<?> registerStudent(@RequestBody @Valid Student student) {
		Student std = srsStudentService.register(student);
		return new ResponseEntity<>(std, HttpStatus.CREATED);
	}

	@PutMapping(STUDENT_ID_API)
	public ResponseEntity<?> updateStudent(@PathVariable @NotNull(message = "{studentId.notEmtpy}") Long studentId,
			 @RequestBody @Valid Student student) {
		student.setId(studentId);
		Student std = srsStudentService.update(studentId, student);
		return new ResponseEntity<>(std, HttpStatus.CREATED);
	}

	@DeleteMapping(STUDENT_ID_API)
	public ResponseEntity<?> deleteStudent(@PathVariable @NotNull(message = "{studentId.notEmtpy}") Long studentId) {
		LOGGER.info("Student id => {}", studentId);
		
		srsStudentService.deregister(studentId);
		return new ResponseEntity<>(new Response(Status.SUCCESS, "Student deregistration is successful."),
				HttpStatus.OK);
	}

	@GetMapping(STUDENTS_API)
	public ResponseEntity<?> findStudents() {
		List<Student> students = srsStudentService.findAll();
		with(students).onClass(Student.class, match().exclude("courses"));
		return new ResponseEntity<>(students, HttpStatus.OK);
	}

	@PostMapping(STUDENTS_API)
	public ResponseEntity<?> registerStudents(
			@RequestBody Set<@Valid Student> students) {
		List<?> studnts = srsStudentService.registerAll(students);
		return new ResponseEntity<>(studnts, HttpStatus.CREATED);
	}

	@PostMapping(ENROLL_COURSE_API)
	public ResponseEntity<?> enrollSutdentForCourses(
			@PathVariable @NotNull(message = "{studentId.notEmtpy}") Long studentId,
			@RequestBody @NotEmpty(message = "{courseid.required}") List<Long> courseIds) {
		if (courseIds.size() > MAX_COURSES_PER_STUDENT)
			throw new SRSProcessingException(ErrorCode.COURSE_LIMIT_EXCEEDS);
		srsStudentService.enrollSutdentForCourses(studentId, courseIds);
		return new ResponseEntity<>(new Response(Status.SUCCESS,
				" Student " + studentId + " enrollment for given Courses is successful."), HttpStatus.OK);
	}

	@PostMapping(ENROLL_STUDENT_API)
	public ResponseEntity<?> enrollCourseForStudents(
			@PathVariable @NotNull(message = "{courseId.notEmtpy}") Long courseId,
			@RequestBody @NotEmpty(message = "{studentid.required}") List<Long> studentIds) {

		if (studentIds.size() > MAX_STUDENTS_PER_COURSE)
			throw new SRSProcessingException(ErrorCode.STUDENT_LIMIT_EXCEEDS);
		srsCourseService.enrollCourseForStudents(courseId, studentIds);
		return new ResponseEntity<>(new Response(Status.SUCCESS,
				" Course " + courseId + "  enrollment for given Students is successful."), HttpStatus.OK);
	}

	@GetMapping(COURSE_ID_API)
	public ResponseEntity<?> findCourse(@PathVariable @NotNull(message = "{courseId.notEmtpy}") Long courseId) {
		Course course = srsCourseService.find(courseId);
		return new ResponseEntity<>(course, HttpStatus.OK);
	}

	@PostMapping(COURSE_API)
	public ResponseEntity<?> registerCourse(@RequestBody @Valid Course course) {
		course = srsCourseService.register(course);
		return new ResponseEntity<>(course, HttpStatus.CREATED);
	}

	@PutMapping(COURSE_ID_API)
	public ResponseEntity<?> updateCourse(@PathVariable @NotNull(message = "{courseId.notEmtpy}") Long courseId,
			@Valid @RequestBody Course course) {
		course.setId(courseId);
		course = srsCourseService.update(courseId, course);
		return new ResponseEntity<>(course, HttpStatus.CREATED);
	}

	@DeleteMapping(COURSE_ID_API)
	public ResponseEntity<?> deleteCourse(@PathVariable @NotNull(message = "{courseId.notEmtpy}") Long courseId) {
		srsCourseService.deregister(courseId);
		return new ResponseEntity<>(new Response(Status.SUCCESS, "Course deregistration is successful."),
				HttpStatus.OK);
	}

	@GetMapping(COURSES_API)
	public ResponseEntity<?> findCourses() {
		List<?> courses = srsCourseService.findAll();
		return new ResponseEntity<>(courses, HttpStatus.OK);
	}

	@PostMapping(COURSES_API)
	public ResponseEntity<?> registerCourses(
			@RequestBody Set<@Valid Course> courses) {
		List<?> crs = srsCourseService.registerAll(courses);
		return new ResponseEntity<>(crs, HttpStatus.CREATED);
	}

	@PostMapping(REPORT_API)
	public ResponseEntity<?> srsFilter(@RequestBody SearchFilter filter) {
		List<?> students = srsStudentService.searchSrsSystem(filter);
		return ResponseEntity.ok(students);
	}
}
