package com.mdi.srs.enums;

import org.springframework.http.HttpStatus;

public enum ErrorCode {

	SYSTEM_ERROR(HttpStatus.INTERNAL_SERVER_ERROR,
			"There was some technical issue for this operation, Please come after sometime."),
	SERVICE_ERROR(HttpStatus.SERVICE_UNAVAILABLE, "System is down at this moment, Please try again later."),
	DATA_NOT_FOUND(HttpStatus.BAD_REQUEST, "No data found with the given information, Please check input data once."),
	INVALID_API_REQUEST(HttpStatus.BAD_REQUEST, "Invalid Use of API Serivce."),
	COURSE_LIMIT_EXCEEDS(HttpStatus.TOO_MANY_REQUESTS, "A student can have only 5 courses registered."),
	STUDENT_LIMIT_EXCEEDS(HttpStatus.TOO_MANY_REQUESTS, "A course can have only 50 students registered."),;

	private HttpStatus status;

	private String message;

	private ErrorCode(HttpStatus status, String message) {
		this.status = status;
		this.message = message;
	}

	public String getMessage() {
		return this.message;
	}

	public HttpStatus getStatus() {
		return this.status;
	}
}
