package com.mdi.srs.enums;

public enum Status {

	SUCCESS("Success"), FAILED("Failed");

	private Status(String message) {
		this.message = message;
	}

	private String message;

	public String getMessage() {
		return this.message;
	}
}
