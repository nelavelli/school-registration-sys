# README #

Running in local environment with h2 database.
----------------------------------------------

Just clone this repo into your local and run SRS (School registration System) with below 2 commands.

# for building the project in local env.
mvn clean install 

#for running the project in local env.
mvn spring-boot:run

-- this project uses H2 database in your local.


Running in container environment with MySQL database.
-----------------------------------------------------


In order to start the application in container,


clone the repository, make sure that you have maven, docker and docker-compose working in the machine that you are planning to SRS app.

Open your command-line terminal and browse to the root directory of this repository where  start.sh file is located. 
Execute the start.sh inside your terminal (./start.sh).

maven build, docker image build and deploy will happen with this & should be able to access all the apis.

Use the API collection file for various APIs and their payloads for SRS.

API Collection file ::: StudentRegistrationSystem.postman_collection.json

Note: have attached single get call screenshot for reference purpose.
